//
//  ListNameServersViewController.m
//  TestForXylarity
//
//  Created by Yurii Balashkevych on 10/18/15.
//  Copyright © 2015 Yurii Balashkevych. All rights reserved.
//

#import "ListNameServersViewController.h"

static NSString *cellIdentifier = @"worldNameCell";

@interface ListNameServersViewController ()

@property (strong, nonatomic) NSArray <World *> *worlds;

@end

@implementation ListNameServersViewController

#pragma mark - Designated Initializer

- (instancetype)initWithStyle:(UITableViewStyle)style andWorlds:(NSArray *)worlds {
    self = [super initWithStyle:style];
    if (self) {
        self.worlds = worlds;
    }
    return self;
}

#pragma mark - Life Cycle

- (void)viewDidLoad {
    [super viewDidLoad];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.worlds count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
        cell.textLabel.text = self.worlds[indexPath.row].name;
    }
    return cell;
}

#pragma mark - Status Bar

- (BOOL)prefersStatusBarHidden {
    return YES;
}

@end
