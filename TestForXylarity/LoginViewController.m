//
//  ViewController.m
//  TestForXylarity
//
//  Created by Yurii Balashkevych on 10/18/15.
//  Copyright © 2015 Yurii Balashkevych. All rights reserved.
//

#import "ListNameServersViewController.h"
#import "LoginViewController.h"
#import "ServerManager.h"
#import "WorldRequest.h"

@interface LoginViewController () <UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet UITextField *loginTextField;
@property (weak, nonatomic) IBOutlet UITextField *passwordTextField;
@property (weak, nonatomic) IBOutlet UIButton *loginButton;

@end

@implementation LoginViewController

#pragma mark - Life Cycle

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Actions

- (IBAction)login:(UIButton *)sender {
    [self.view endEditing:YES];
    NSDictionary *parametersForRequest = [WorldRequest requestWithLogin:self.loginTextField.text andPassword:self.passwordTextField.text];
    [[ServerManager sharedManager] postWorldsWithParameters:parametersForRequest onSuccess:^(NSArray *worlds) {
        ListNameServersViewController *vc = [[ListNameServersViewController alloc] initWithStyle:UITableViewStylePlain andWorlds:worlds];
        [self.navigationController pushViewController:vc animated:YES];
    } andFailure:^(NSError *error) {
        if (error) {
            NSLog(@"%@", [error localizedDescription]);
        }
    }];
}

#pragma mark - UITextFieldDelegate

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    if ([string isEqualToString:@""]) {
        self.loginButton.enabled = NO;
    } else {
        NSString *text = [textField.text stringByReplacingCharactersInRange:range withString:string];
        if ([textField isEqual:self.loginTextField]) {
            self.loginButton.enabled = ![text isEqualToString:@""] && ![self.passwordTextField.text isEqualToString:@""];
        } else if ([textField isEqual:self.passwordTextField]) {
            self.loginButton.enabled = ![self.loginTextField.text isEqualToString:@""] && ![text isEqualToString:@""];
        }
    }
    return YES;
}

@end
