//
//  ServerManager.m
//  TestForXylarity
//
//  Created by Yurii Balashkevych on 10/18/15.
//  Copyright © 2015 Yurii Balashkevych. All rights reserved.
//

#import <AFNetworking.h>

#import "ServerManager.h"
#import "WorldStatus.h"
#import "World.h"

@interface ServerManager ()

@property (strong, nonatomic) AFHTTPRequestOperationManager *requestManager;
@property (strong, nonatomic) NSURL *baseURL;

@end

@implementation ServerManager

#pragma mark - Shared Instance

+ (instancetype)sharedManager {
    static ServerManager *manager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        manager = [[ServerManager alloc] init];
    });
    return manager;
}

#pragma mark - Request methods

- (void)postWorldsWithParameters:(NSDictionary *)params onSuccess:(void(^)(NSArray *worlds))success andFailure:(void(^)(NSError *error))failure {
    [self.requestManager POST:@"worlds" parameters:params success:^(AFHTTPRequestOperation * _Nonnull operation, id  _Nonnull responseObject) {
        NSArray *allAvailableWorlds = [responseObject valueForKey:@"allAvailableWorlds"];
        NSMutableArray *worlds = [NSMutableArray array];
        for (id item in allAvailableWorlds) {
            World *world = [[World alloc] initWithParameters:item];
            [worlds addObject:world];
        }
        if (success) {
            success(worlds);
        }
    } failure:^(AFHTTPRequestOperation * _Nonnull operation, NSError * _Nonnull error) {
        if (failure) {
            failure(error);
        }
    }];
}

#pragma mark - Lazy Instatiation

- (AFHTTPRequestOperationManager *)requestManager {
    if (!_requestManager) {
        _requestManager = [[AFHTTPRequestOperationManager alloc] initWithBaseURL:self.baseURL];
        _requestManager.responseSerializer = [AFPropertyListResponseSerializer serializer];
    }
    return _requestManager;
}

- (NSURL *)baseURL {
    if (!_baseURL) {
        _baseURL = [NSURL URLWithString:@"http://backend1.lordsandknights.com/XYRALITY/WebObjects/BKLoginServer.woa/wa/"];
    }
    return _baseURL;
}

@end
