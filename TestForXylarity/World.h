//
//  World.h
//  TestForXylarity
//
//  Created by Yurii Balashkevych on 10/18/15.
//  Copyright © 2015 Yurii Balashkevych. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "WorldStatus.h"

@interface World : NSObject

@property (strong, nonatomic) NSString *countryCode;            // country = ISO country code string
@property (strong, nonatomic) NSString *uniqueIdentifier;       // id = a unique identifier
@property (strong, nonatomic) NSString *langID;                 // language = ISO language code string
@property (strong, nonatomic) NSString *name;                   // name = Name string of the world
@property (strong, nonatomic) NSURL *mapURL;                    // mapURL = A URL to a map server
@property (strong, nonatomic) NSURL *serverURL;                 // url = A URL to the world server
@property (strong, nonatomic) WorldStatus *worldStatus;

- (instancetype)initWithParameters:(NSDictionary *)params;

@end
