//
//  WorldStatus.m
//  TestForXylarity
//
//  Created by Yurii Balashkevych on 10/18/15.
//  Copyright © 2015 Yurii Balashkevych. All rights reserved.
//

#import "WorldStatus.h"

@implementation WorldStatus

#pragma mark - Designated Initializer

- (instancetype)initWithParameters:(NSDictionary *)params {
    self = [super init];
    if (self) {
        self.statusDescription = [params valueForKey:@"description"];
        self.identifier = [params valueForKey:@"id"];
    }
    return self;
}

@end
