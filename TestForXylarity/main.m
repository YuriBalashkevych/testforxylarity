//
//  main.m
//  TestForXylarity
//
//  Created by Yurii Balashkevych on 10/18/15.
//  Copyright © 2015 Yurii Balashkevych. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
