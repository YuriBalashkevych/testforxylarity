//
//  WorldRequest.m
//  TestForXylarity
//
//  Created by Yurii Balashkevych on 10/18/15.
//  Copyright © 2015 Yurii Balashkevych. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "WorldRequest.h"

@implementation WorldRequest

+ (NSDictionary *)requestWithLogin:(NSString *)login andPassword:(NSString *)password {
    return @{@"login":login,
             @"password":password,
             @"deviceType":[NSString stringWithFormat:@"%@ - %@ %@", [[UIDevice currentDevice] model], [[UIDevice currentDevice] systemName], [[UIDevice currentDevice] systemVersion]],
             @"deviceId":[[NSUUID UUID] UUIDString]};
}

@end
