//
//  WorldStatus.h
//  TestForXylarity
//
//  Created by Yurii Balashkevych on 10/18/15.
//  Copyright © 2015 Yurii Balashkevych. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface WorldStatus : NSObject

@property (strong, nonatomic) NSString *statusDescription;  // description = Description string of a status
@property (strong, nonatomic) NSString *identifier;         // id = Identifier indicating the status

- (instancetype)initWithParameters:(NSDictionary *)params;

@end
