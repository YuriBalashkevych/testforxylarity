//
//  World.m
//  TestForXylarity
//
//  Created by Yurii Balashkevych on 10/18/15.
//  Copyright © 2015 Yurii Balashkevych. All rights reserved.
//

#import "World.h"

@implementation World

#pragma mark - Designated Initializer

- (instancetype)initWithParameters:(NSDictionary *)params {
    self = [super init];
    if (self) {
        self.countryCode = [params valueForKey:@"country"];
        self.uniqueIdentifier = [params valueForKey:@"id"];
        self.langID = [params valueForKey:@"language"];
        self.mapURL = [NSURL URLWithString:[NSString stringWithFormat:@"%@", [params valueForKey:@"mapURL"]]];
        self.serverURL = [NSURL URLWithString:[NSString stringWithFormat:@"%@", [params valueForKey:@"url"]]];
        self.name = [params valueForKey:@"name"];
        self.worldStatus = [[WorldStatus alloc] initWithParameters:[params valueForKey:@"worldStatus"]];
    }
    return self;
}

@end
