//
//  ListNameServersViewController.h
//  TestForXylarity
//
//  Created by Yurii Balashkevych on 10/18/15.
//  Copyright © 2015 Yurii Balashkevych. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "World.h"

@interface ListNameServersViewController : UITableViewController

- (instancetype)initWithStyle:(UITableViewStyle)style andWorlds:(NSArray *)worlds;

@end
