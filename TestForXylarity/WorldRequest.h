//
//  WorldRequest.h
//  TestForXylarity
//
//  Created by Yurii Balashkevych on 10/18/15.
//  Copyright © 2015 Yurii Balashkevych. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface WorldRequest : NSObject

+ (NSDictionary *)requestWithLogin:(NSString *)login andPassword:(NSString *)password;

@end
