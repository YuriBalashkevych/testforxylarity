//
//  ServerManager.h
//  TestForXylarity
//
//  Created by Yurii Balashkevych on 10/18/15.
//  Copyright © 2015 Yurii Balashkevych. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ServerManager : NSObject

+ (instancetype)sharedManager;

- (void)postWorldsWithParameters:(NSDictionary *)params onSuccess:(void(^)(NSArray *worlds))success andFailure:(void(^)(NSError *error))failure; //response - allAvailableWorlds

@end
